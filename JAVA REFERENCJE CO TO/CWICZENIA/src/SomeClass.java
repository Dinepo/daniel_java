public class SomeClass {
    private Integer liczbaA = 3;

    public Integer getLiczbaA() {
        return liczbaA;
    }

    public   void setLiczbaA(Integer liczbaA) {
        this.liczbaA = liczbaA;
    }
      static void testPrymitywny()
    {
        int a = 1;   // Deklarujemy zmienną ‘a’ typu prostego ‘int’ i przypisujemy jej wartość literału ‘1’
        int b =a; // Mamy b czyli kolejne 4 bajty pamięci i zapisaną w nich bitową reprezentację liczby 1, bo taka była wartość zmiennej ‘a’
        a = 1000;

        System.out.println("nie ma zmiany, sluchaj: b odziedziczyło 1 i dalej jest : "+b + " mimo ze zminilismy a bo b utworzylo sobie nowa osobna  komorke a my zmienlismy a");

    }

    static void testReferencji()

    {

        SomeClass klaskaa = new SomeClass();
       // SomeClass klaskab = klaskaa;                     tak tez mozna
        SomeClass klaskab = new SomeClass();
        klaskab = klaskaa;

        klaskaa.setLiczbaA(5000);

        System.out.println("Tu działa referencja w obiekcie jest zmiania bylo" + "3"+ " a jest : " +klaskab.getLiczbaA() + " bo obie wskazuja na ten sam obiekt przy referencji");



    }
    public static void main (String[] args) {
          SomeClass wyzwalaczseta = new SomeClass();

          wyzwalaczseta.setLiczbaA(9);
        testPrymitywny();
        testReferencji();

        int a=43;

        System.out.println(a%11);  // tak dziala modulo, szczegoly w udemy operatory arytmetyczne
    }
}
