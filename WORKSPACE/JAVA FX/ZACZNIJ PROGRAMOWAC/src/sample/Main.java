package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 500, 475));// nie działało rozszerzenie ale tutaj w scenie powiekszamy jak mamy scene


        primaryStage.setWidth(400);
        primaryStage.setHeight(400);
       // primaryStage.setResizable(false);

        //primaryStage.setFullScreen(true);

        primaryStage.setX(0);
        primaryStage.setY(0);
        primaryStage.setOpacity(0.9);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.show();

    }



    public static void main(String[] args) {
        launch(args);
    }
}
