package sample;


import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public Button btnButton;
    public TextField txtName;
    public Label lblName;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void message(ActionEvent actionEvent) {
        String name = txtName.textProperty().get();
        lblName.textProperty().set("Your name is: "+name);
    }


}
