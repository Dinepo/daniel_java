import java.util.Scanner;

public class main {
    public static void main (String[] args)
    {
        int a = 8;


 if (a<8)
     System.out.println( "a<10");
 else if (a==8) // jezeli na gorze bedzie prawdziwe twierdzenie to tutaj sie juz nie pokaze.
     System.out.println("a=8");
 else if (a==7) // jezeli na gorze bedzie prawdziwe twierdzenie to tutaj sie juz nie pokaze.
     System.out.println("a=7");
 else if (a==6) // jezeli na gorze bedzie prawdziwe twierdzenie to tutaj sie juz nie pokaze.
     System.out.println("a=6");
 else if (a==5) // jezeli na gorze bedzie prawdziwe twierdzenie to tutaj sie juz nie pokaze.
     System.out.println("a=5");
 else
     System.out.println("żaden");

 int b=10; // jezeli chcemy by kazda z instrukcji była sprawdzana dajemy samo if:

        if (b<8)
            System.out.println( "b<10");
        if (b==8)
            System.out.println("b=8");
        if (b<17)
            System.out.println("b<17");
        if (b<16)
            System.out.print("b<16");
        if (b<15)
            System.out.print("b<15");
        else
            System.out.print("żaden");
        // zagniezdzanie

        int c=22; // jezeli chcemy by kazda z instrukcji była sprawdzana dajemy samo if:

        if (c<28) // zagniezdzanie w Javie tak jakby zastepuje and

        {System.out.println( "c<28");
          if (c==22)
              System.out.println("c!=21");
          else System.out.println("c=20");}

      // instruckja switch case

        int d= 70;
        switch (d)
        { case 69:
            System.out.println("70");
            break;
            case 78:
                System.out.println("70");
                break;
            case 71:
                System.out.println("71");
                break;
            default:
            System.out.println("Zaden z casów");

            int e=10;  // rzadko stosowane zapytanie
            int f;
            f= e<10 ? 1:-1;
                System.out.println(f);

                /* BREAK I CONTINUE */

                for (int i=0; i<10; i++ )
                {
                    System.out.println(i + " i ");
                if (i==5)
                break;
        }

                System.out.println();
                for (int y=0; y<10; y++ )
                {

                    if (y%2==0)
                        continue;
                    System.out.println(y+ "  Y ");
                }

                // s c a n n e r

                Scanner moj_scanner = new Scanner (System.in);

                String imie = (moj_scanner.next()   );
                System.out.println(imie);

                // s c a n n e r      -      wczytywanie liczb

                Scanner moj_scanner_liczba = new Scanner (System.in);

                int wynik_1 = (moj_scanner.nextInt(   ));
                int wynik_2 = (moj_scanner.nextInt(   ));

                int wynik_main = wynik_1 * wynik_2;


                System.out.println(wynik_main);

// zachowanie sie println : (zeby zobaczyc najpierw wypełjin pola z półki wyzej

                System.out.println('a'+'A');
                System.out.println(1+2);
                System.out.println(1.0+2.0);
                System.out.println("cudzysłów \"");
                System.out.println(true);

// time: 9.50
                //time 16 - pobieranie danych
    }
}}
