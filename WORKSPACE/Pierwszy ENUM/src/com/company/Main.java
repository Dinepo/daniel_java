package com.company;

import java.time.Month;
import java.time.ZonedDateTime;

public class Main {

    public static void main(String[] args) {
	// write your code here
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        System.out.println(zonedDateTime);

        Month month = Month.of(2);
        System.out.println(month);

        zonedDateTime = (ZonedDateTime) month.adjustInto(zonedDateTime);
        System.out.println(zonedDateTime);

    }
}
