import javafx.event.ActionEvent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class KONWER_FARAN_CELS extends JFrame implements ActionListener {
    private JLabel lCelsius, lFahrenheit;
    private JTextField tCelsius, tFahrenheit;
    private JButton bKonwertuj;
    private JCheckBox chWielkie;
    private double tempCelsius, tempFahrenheit;
    private ButtonGroup bgRozmiar;
    private JRadioButton rbMały, rbSredni, rbDuzy;

    private ButtonGroup RadioPanel;
    private JRadioButton rbCtoF, rbFtoC;


    // zmienne na górze a poniżej konstruktor
    public KONWER_FARAN_CELS() {


        setSize(1000, 800);
        setTitle("Przeliczanie stopni z Celsjusza na Fahrenheit");
        setLayout(null);
        // ramka
        lCelsius = new JLabel("Stopnie Celsjusza");
        lCelsius.setBounds(150, 50, 200, 20);
        lCelsius.setVisible(true);
        add(lCelsius);
        // label 1
        lFahrenheit = new JLabel("Stopnie Fahrenheit");
        lFahrenheit.setBounds(150, 100, 200, 20);
        lFahrenheit.setVisible(true);
        add(lFahrenheit);
        // label 2
        tCelsius = new JTextField("");
        tCelsius.setBounds(300, 50, 200, 20);
        tCelsius.setVisible(true);
        add(tCelsius);
        tCelsius.addActionListener(this);
        tCelsius.setToolTipText("Podaj temperaturę w stopniach celsjusza");
        // actionListener dodany do pola textowego aby enter wykonywal zadanie
        // text 1
        tFahrenheit = new JTextField("");
        tFahrenheit.setBounds(300, 100, 200, 20);
        tFahrenheit.setVisible(true);
        tFahrenheit.addActionListener(this);
        add(tFahrenheit);

        // tFahrenheit.setFont(new Font("SansSerif", Font.BOLD,20));

        // text 2
        bKonwertuj = new JButton("KONWERTUJ");
        bKonwertuj.setBounds(200, 150, 200, 20);
        bKonwertuj.setVisible(true);
        // bKonwertuj.setForeground(new Color(33,99,66));
        add(bKonwertuj);

        bKonwertuj.addActionListener(this);


        chWielkie = new JCheckBox("Wielkie litery");
        chWielkie.setBounds(400, 150, 200, 20);
        add(chWielkie);
        //chWielkie.addActionListener(this); // zakomentowalem dla filmu o zastapieniu actionlistera przez if

        bgRozmiar = new ButtonGroup();
        rbMały = new JRadioButton("Mały", true);
        rbMały.setBounds(200, 200, 100, 20);
        bgRozmiar.add(rbMały);
        add(rbMały);
        rbMały.addActionListener(this);


        rbSredni = new JRadioButton("Sredni", false);
        rbSredni.setBounds(300, 200, 100, 20);
        bgRozmiar.add(rbSredni);
        add(rbSredni);
        rbSredni.addActionListener(this);

        rbDuzy = new JRadioButton("Duzy", false);
        rbDuzy.setBounds(400, 200, 100, 20);
        bgRozmiar.add(rbDuzy);
        add(rbDuzy);
        rbDuzy.addActionListener(this);

        // radio gruop dla faranheit celsius


        RadioPanel = new ButtonGroup();
        rbCtoF = new JRadioButton("CelstoFahr", true);
        rbCtoF.setBounds(200, 250, 100, 20);
        RadioPanel.add(rbCtoF);
        add(rbCtoF);
        rbCtoF.addActionListener(this);

        rbFtoC = new JRadioButton("FahrtoCels", false);
        rbFtoC.setBounds(300, 250, 100, 20);
        RadioPanel.add(rbFtoC);
        add(rbFtoC);
         rbFtoC.addActionListener(this);

    }

    public static void main(String[] args) {
        KONWER_FARAN_CELS aplikacja = new KONWER_FARAN_CELS();

        aplikacja.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        aplikacja.setVisible(true);

    }


    @Override
    public void actionPerformed(java.awt.event.ActionEvent e) {
        Object źródło = e.getSource();
        if (źródło == bKonwertuj || źródło == tFahrenheit || źródło == tCelsius) {
            // w filmie 10 jest to rozpisane, ale ja swoim sposobem na gorze to zrpbiłem

            // wstawiam na podstawie filmu 9
            if (chWielkie.isSelected() == true) {
                tFahrenheit.setFont(new Font("SANS_SERIF", Font.BOLD, 10));
            } else {
                tFahrenheit.setFont(new Font("SANS_SERIF", Font.PLAIN, 10));
            }

            // wstawiam na potrzeby filmu 10
            if (rbCtoF.isSelected()) {
                tempCelsius = Double.parseDouble(tCelsius.getText());
                tempFahrenheit = 32.0 + (9.0 / 5.0) * tempCelsius;
                tFahrenheit.setText(String.valueOf(tempFahrenheit));
            } else if (rbFtoC.isSelected()) {
                tempFahrenheit = Double.parseDouble(tFahrenheit.getText());
                 tempCelsius= (tempFahrenheit -32)*(5.0 / 9.0);
                 tCelsius.setText(String.valueOf(tempCelsius));}
            }










             else if (źródło == rbMały) {
                tFahrenheit.setFont(new Font("SANS_SERIF", Font.BOLD, 10));
            } else if (źródło == rbSredni) {
                tFahrenheit.setFont(new Font("SANS_SERIF", Font.BOLD, 12));
            } else if (źródło == rbDuzy) {
                tFahrenheit.setFont(new Font("SANS_SERIF", Font.BOLD, 14));
            }
            // else if(źródło == chWielkie){ // zakomentowalem dla filmu o zastapieniu actionlistera przez if (film 9)
            //   if (chWielkie.isSelected()==true){
            //      tFahrenheit.setFont(new Font("SANS_SERIF",Font.BOLD, 6));}
            // else {
            //  tFahrenheit.setFont(new Font("SANS_SERIF",Font.PLAIN, 6 ));}
        }
    }



