public class JakiWazonik {

    int wysokosc;
    int szerokosc;
    String Kolor;

    String rozbij_wazon;

    public JakiWazonik(int awysokosc,int aszerokosc, String aKolor )
    {
        wysokosc=awysokosc;
        szerokosc=aszerokosc;
        Kolor=aKolor;

    }

    public int getSzerokosc() {
        return szerokosc;
    }

    public String getKolor() {
        return Kolor;
    }

    public int getWysokosc() {
        return wysokosc;
    }

    public void setRozbij_wazon(String rozbij_wazon) {
        this.rozbij_wazon = rozbij_wazon;
    }

    public String getRozbij_wazon() {
        return rozbij_wazon;
    }
}
