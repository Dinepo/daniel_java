public class Home {


    public static void main(String args[]) {


        TV nosy = new TV();


        TV sama = new TV(32, "sa,ma", "Daniel jest wlascicielem");



        System.out.println(sama.getName() + "   " + sama.getSize() + "  " + sama.getDaniel());
        System.out.println(nosy.getName() + "   " + nosy.getSize());



        // od tego momentu nie korzystasz z konstruktora, w delphi masz zle
        // metoda wlacz wylacz telewizor
        sama.getOnOFF(); // tu automatycznie przypisuje false
        sama.setOnOFF(false);
        sama.getOnOFF();
        sama.setOnOFF(true);
        sama.getOnOFF();


        pies reks = new pies();
        reks.speak();

        reks.setSize(15);
        System.out.println(reks.getSize());

    }
}