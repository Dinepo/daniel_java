import com.sun.org.apache.bcel.internal.generic.NEW;

import javax.swing.*;
import javax.xml.crypto.Data;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

public class Okno extends JFrame implements ActionListener {
    JButton bPodajDate, bWyjście, bMoj;
    JLabel lWyswietldate;

    public Okno() {
        setSize(800, 600);
        setTitle("Moje Pierwsze Okienko");
        setLayout(null);
        bPodajDate = new JButton("Podaj datę");
        bPodajDate.setBounds(100, 50, 150, 20);
        add(bPodajDate);
        bPodajDate.addActionListener(this);

        bWyjście = new JButton("Wyjście");
        bWyjście.setBounds(250, 50, 150, 20);
        add(bWyjście);
        bWyjście.addActionListener(this);
        // moj button do usuniecia
        bMoj = new JButton("Mój_button");
        bMoj.setBounds(550, 50, 150, 20);
        add(bMoj);
        bMoj.addActionListener(this);
        lWyswietldate = new JLabel("data:");
        lWyswietldate.setBounds(150,100,250,20);
        //lWyswietldate.setForeground(Color.PINK);
        //lWyswietldate.setForeground(new Color(339966));
        lWyswietldate.setForeground(new Color(33,99,66));
        lWyswietldate.setFont(new Font("SansSerif", Font.BOLD,20));

        add(lWyswietldate);
    }


    public static void main(String[] args) {
        Okno okienko = new Okno();
        okienko.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okienko.setVisible(true);

    }


    @Override
    public void actionPerformed(ActionEvent e) {
        Object zrodlo = e.getSource();
        if (zrodlo == bPodajDate) {
            lWyswietldate.setText(new Date().toString());
           // System.out.println(new Date());
        }

        // else if ( // odkomentowac
        if (
                zrodlo == bWyjście) {
            dispose();
        }
        if (zrodlo == bMoj) {
            System.out.println("wygrałem");

        }
    }
}



