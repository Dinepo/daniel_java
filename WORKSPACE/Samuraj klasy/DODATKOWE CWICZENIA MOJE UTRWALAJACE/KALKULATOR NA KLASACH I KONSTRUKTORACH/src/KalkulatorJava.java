import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

public class KalkulatorJava extends JFrame implements ActionListener  // dodajemy jframe
// pierwsza rzecz to tworzymy konstruktor
{
    private JButton bPrzycisklicz, bZeruj;
    private JLabel lWyswietlWynik;
    private JLabel lWyswietldate;
    private JTextField tPierwszaLiczba, tDrugaLiczba;
    private double liczba1, liczba2;
    private ButtonGroup bgOperacja;
    private JRadioButton rbMnozenie, rbDodawanie, rbOdejmowanie, rbDzielenie;

public KalkulatorJava()
{
    setSize(400,300);
    setTitle("Kalkulator zrobiony w Javie");
     setLayout(null);

    bPrzycisklicz = new JButton("POLICZ");
    bPrzycisklicz.setBounds( 100,100, 100, 20);
    bPrzycisklicz.addActionListener(this);
    add(bPrzycisklicz);


   bZeruj = new JButton("WYZERUJ");
   bZeruj.setBounds(200,100,100,20);
   bZeruj.addActionListener(this);
   add(bZeruj);

   lWyswietlWynik = new JLabel("WYNIK TO: " );
   lWyswietlWynik.setBounds(150, 150, 100,20);
   add(lWyswietlWynik);

    lWyswietldate = new JLabel("DATA TO: " );
    lWyswietldate.setBounds(100, 200, 300,20);
    add(lWyswietldate);

    tPierwszaLiczba= new JTextField("0");
    tPierwszaLiczba.setBounds(100, 50, 100, 20);
    tPierwszaLiczba.setVisible(true);
    add(tPierwszaLiczba);
    tPierwszaLiczba.addActionListener(this);
    tPierwszaLiczba.setToolTipText("Podaj pierwsza liczbe");

    tDrugaLiczba= new JTextField("0");
    tDrugaLiczba.setBounds(200, 50, 100, 20);
    tDrugaLiczba.setVisible(true);
    add(tDrugaLiczba);
    tDrugaLiczba.addActionListener(this);
    tDrugaLiczba.setToolTipText("Podaj druga liczbe");
    bgOperacja = new ButtonGroup();

    rbDodawanie = new JRadioButton("DODAJ", true);
    rbDodawanie.setBounds(10,20,80,20);
    bgOperacja.add(rbDodawanie);
    add(rbDodawanie);
    //rbDodawanie.addActionListener(this);


    rbOdejmowanie = new JRadioButton("ODEJMIJ", false);
    rbOdejmowanie.setBounds(100,20,80,20);
    bgOperacja.add(rbOdejmowanie);
    add(rbOdejmowanie);
    //rbOdejmowanie.addActionListener(this);


    rbMnozenie = new JRadioButton("POMNÓŻ", false);
    rbMnozenie.setBounds(200,20,80,20);
    bgOperacja.add(rbMnozenie);
    add(rbMnozenie);
    //rbMnozenie.addActionListener(this);

    rbDzielenie = new JRadioButton("PODZIEL", false);

    rbDzielenie.setBounds(300,20,80,20);
    bgOperacja.add(rbDzielenie);
    add(rbDzielenie);
    //rbDzielenie.addActionListener(this);
}
    public static void main (String[] args)
    { KalkulatorJava Kalkulator = new KalkulatorJava();
    Kalkulator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    Kalkulator.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        Object source = e.getSource();  // tu masz 3 przyklady konwersji

        if (source== bPrzycisklicz ){
        if (rbMnozenie.isSelected()) {
            //System.out.println(new Date());
            liczba1 = Double.parseDouble(tPierwszaLiczba.getText()); // konwersja do zmiennej z edita
            liczba2 = Double.parseDouble(tDrugaLiczba.getText());

            Mnozenie pomnoz = new Mnozenie(liczba1, liczba2);
            lWyswietlWynik.setText(String.valueOf(pomnoz.obliczMnozenie())); // konwersja do labelki z klasy
            lWyswietldate.setText(new Date().toString());}// konwersja do labelki daty.}

          else if   (rbDodawanie.isSelected()) {
                //System.out.println(new Date());
                liczba1 = Double.parseDouble(tPierwszaLiczba.getText()); // konwersja do zmiennej z edita
                liczba2 = Double.parseDouble(tDrugaLiczba.getText());

                Dodawanie dodaj = new Dodawanie(liczba1, liczba2);
                lWyswietlWynik.setText(String.valueOf(dodaj.obliczDodawanie())); // konwersja do labelki z klasy
                lWyswietldate.setText(new Date().toString());}// konwersja do labelki daty.
        else if   (rbOdejmowanie.isSelected()) {
            //System.out.println(new Date());
            liczba1 = Double.parseDouble(tPierwszaLiczba.getText()); // konwersja do zmiennej z edita
            liczba2 = Double.parseDouble(tDrugaLiczba.getText());

            Odejmowanie odejmnij = new Odejmowanie(liczba1, liczba2);
            lWyswietlWynik.setText(String.valueOf(odejmnij.obliczOdejmowanie())); // konwersja do labelki z klasy
            lWyswietldate.setText(new Date().toString());}
        else if   (rbDzielenie.isSelected()) {
            //System.out.println(new Date());
            liczba1 = Double.parseDouble(tPierwszaLiczba.getText()); // konwersja do zmiennej z edita
            liczba2 = Double.parseDouble(tDrugaLiczba.getText());

            Dzielenie podziel = new Dzielenie(liczba1, liczba2);
            lWyswietlWynik.setText(String.valueOf(podziel.obliczDzielenie())); // konwersja do labelki z klasy
            lWyswietldate.setText(new Date().toString());}

        } else if (source == bZeruj) {
            tPierwszaLiczba.setText("0");
            tDrugaLiczba.setText("0");

        }
    }
}
