import com.sun.org.apache.xalan.internal.lib.Extensions;

import javax.swing.*;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.Document;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;
import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.event.*;

import static javafx.scene.paint.Color.rgb;

public class ZamienNumerWagonu extends JFrame implements ActionListener  // layuot manaager: http://www.jsystems.pl/blog/artykul.html?id=108
{
    JTextArea notatnik;
    JButton zamien;
    JButton wklej;
    JButton kopiuj;
    JMenuBar menuBar;
    JMenu menuOpcje;
    JMenuItem mCofnij, mPonow;
    protected UndoManager undoManager = new UndoManager();

    public void updateButtons() {
        // mCofnij.setText(mCofnij.getUndoPresentationName());
        // mPonow.setText(mCofnij.getRedoPresentationName());
        mCofnij.setEnabled(undoManager.canUndo());
        mPonow.setEnabled(undoManager.canRedo());
    }

    public ZamienNumerWagonu() {

        setTitle("PROGRAM DO ZAMIANY CYFR WAGONU");
        setLocation(500, 0);
        setSize(600, 900);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        menuOpcje = new JMenu("OPCJE");

        mCofnij = new JMenuItem("COFNIJ", 'C'); // mnemonic w pojedynczych apostrofach
        mCofnij.addActionListener(this);
        mPonow = new JMenuItem("PONÓW", 'P');
        mPonow.addActionListener(this);
        menuOpcje.add(mCofnij);
        menuOpcje.add(mPonow);


        notatnik = new JTextArea();
        JScrollPane sp = new JScrollPane(notatnik);
        sp.setBounds(0, 100, 350, 600);
        Font font = new Font("Verdana", Font.BOLD, 12);
        notatnik.setFont(font);
        notatnik.setForeground(Color.BLUE);
        notatnik.setToolTipText("Aby wkleić wagony wciśnij CTRL + V");
        add(sp);
        notatnik.setVisible(true);


        zamien = new JButton("ZAMIEŃ NUMER");
        zamien.setBounds(0, 50, 350, 50);
        zamien.setBackground(new Color(166, 166, 166));
        zamien.setFont(new Font("Courier New", Font.ITALIC, 32)); //http://www.java2s.com/Tutorial/Java/0240__Swing/SetFontandforegroundcolorforaJLabel.htm
        zamien.setForeground(Color.blue);  // https://stackoverflow.com/questions/15393385/how-to-change-text-color-of-a-jbutton
        add(zamien);
        zamien.setVisible(true);
        zamien.addActionListener(this);

        wklej = new JButton("WKLEJ");
        wklej.setBounds(0, 0, 175, 50);
        wklej.setBackground(new Color(166, 166, 166));
        wklej.setFont(new Font("Courier New", Font.ITALIC, 32)); //http://www.java2s.com/Tutorial/Java/0240__Swing/SetFontandforegroundcolorforaJLabel.htm
        wklej.setForeground(Color.green);  // https://stackoverflow.com/questions/15393385/how-to-change-text-color-of-a-jbutton
        add(wklej);
        wklej.setVisible(true);
        wklej.addActionListener(this);

        kopiuj = new JButton("WYTNIJ");
        kopiuj.setBounds(175, 0, 175, 50);
        kopiuj.setBackground(new Color(166, 166, 166));
        kopiuj.setFont(new Font("Courier New", Font.ITALIC, 32)); //http://www.java2s.com/Tutorial/Java/0240__Swing/SetFontandforegroundcolorforaJLabel.htm
        kopiuj.setForeground(Color.green);  // https://stackoverflow.com/questions/15393385/how-to-change-text-color-of-a-jbutton
        add(kopiuj);
        kopiuj.setVisible(true);
        kopiuj.addActionListener(this);
        mPonow.setEnabled(false);
        mCofnij.setEnabled(false);  // ostatnia próba https://www.java-forums.org/javax-swing/9570-undo-redo-jtextarea.html
        menuBar = new JMenuBar();

        setJMenuBar(menuBar);
        menuBar.add(Box.createVerticalGlue()); // nie wiem do czego słuzy
        menuBar.add(menuOpcje);
    }


    public static void main(String[] args) {

        ZamienNumerWagonu zmienNumer = new ZamienNumerWagonu();
        zmienNumer.setVisible(true);


    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object z = e.getSource();
        String srt = "";
        if (z == zamien) {
//            Scanner scaner = new Scanner(notatnik.getText());
//            while (scaner.hasNext()){
//                srt= (scaner.nextLine() );}
            srt = notatnik.getText();
            srt = srt.replaceAll("-", "");


            notatnik.setText(srt);
        }
        final Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
        if (z == wklej) {
            Transferable clipdata = clip.getContents(clip);

            try {
                if (clipdata.isDataFlavorSupported(DataFlavor.stringFlavor)) { // jesli damy imageFlavour to wkleja obraz
                    String s = (String) (clipdata.getTransferData(DataFlavor.stringFlavor));
                    notatnik.replaceSelection(s);
                }


            } catch (UnsupportedFlavorException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }


        }
        if (z == kopiuj)

        {                                   //https://www.youtube.com/watch?v=FCXLPoAX_Xc  super film o kopiuj i wklej
            notatnik.selectAll();  //https://kodejava.org/how-do-i-select-all-text-in-jtextarea/
            String selected = notatnik.getSelectedText();
            StringSelection data = new StringSelection(selected);
            clip.setContents(data, data);
            notatnik.replaceRange("", notatnik.getSelectionStart(), notatnik.getSelectionEnd());

        }
//        UndoManager undoManager = new UndoManager();
//
//        Document document = notatnik.getDocument();
//        document.addUndoableEditListener(new UndoableEditListener() {
//            @Override
//            public void undoableEditHappened(UndoableEditEvent e) {
//                undoManager.addEdit(e.getEdit());
//            }
//        });
        notatnik.getDocument().addUndoableEditListener(
                new UndoableEditListener() {
                    public void undoableEditHappened(UndoableEditEvent e) {
                        undoManager.addEdit(e.getEdit());
                        updateButtons();
                    }
                });

        if (z == mCofnij)// {

//            try {
//                undoManager.undo();
//            } catch (CannotUndoException cue) {
//            }
        // }
        {
            try {
                undoManager.undo();
            } catch (CannotRedoException cre) {
                cre.printStackTrace();
            }
            updateButtons();

        }
        ;

        if (z == mPonow)// {
//            {
////                try {
////                    undoManager.redo();
////                } catch (CannotRedoException cre) {
////                }
//            }
//            ;
//
//
//            // nie działa linki: https://hakusjavablog.wordpress.com/2012/04/01/cofnij-powtorz-w-java/
//            //https://docs.oracle.com/javase/7/docs/api/javax/swing/undo/UndoManager.html
//            //https://stackoverflow.com/questions/2547404/using-undo-and-redo-for-jtextarea
//
//
//        }

        {
            try {
                undoManager.redo();
            } catch (CannotRedoException cre) {
                cre.printStackTrace();
            }
            updateButtons();
        }
    }

    ;




}
// ostatnia próba https://www.java-forums.org/javax-swing/9570-undo-redo-jtextarea.html TEN DZIALA !!!!! :)))))

