import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RownanieKwadratowe extends JFrame implements ActionListener

{
private JTextField tA, tB, tC, twynik;
private JLabel lA, lB, lC;

private JButton bWyjscie, bRozwiaz;





    public RownanieKwadratowe(){

       setSize(600,800);
       setTitle("Rozwiązanie równania kwadratowego");
       setLayout(null);
       //setBackground(new Color(66, 134, 244)); // nie dziala
       // https://4programmers.net/Forum/Java/196718-zmiana_koloru_panelu_w_innej_klasie
        getContentPane().setBackground(new Color(66, 134, 244));

        // tak dziala a tu jest link: https://stackoverflow.com/questions/1081486/setting-background-color-for-the-jframe

       lA = new JLabel("a",JLabel.RIGHT);  // Jlabel.Right chyba powoduje, ze a trzyma sie prawej strony labela
       lA.setBounds(10,50,30,20);

       tA = new JTextField();
       tA.setBounds(50,50,50,20);
       tA.setToolTipText("Podaj współczynnik a");

        lB = new JLabel("b",JLabel.RIGHT);
        lB.setBounds(110,50,30,20);

        tB = new JTextField();
        tB.setBounds(150,50,50,20);
        tB.setToolTipText("Podaj współczynnik b");
        lC = new JLabel("c",JLabel.RIGHT);
        lC.setBounds(210,50,30,20);

        tC = new JTextField();
        tC.setBounds(250,50,50,20);
        tC.setToolTipText("Podaj współczynnik c");

        bRozwiaz = new JButton("Rozwiąż równanie");
        bRozwiaz.setBounds(50,100,150,20);;
        bRozwiaz.addActionListener(this);
        bWyjscie = new JButton("WYJŚCIE");
        bWyjscie.setBounds(250,100,100,20);;
        bWyjscie.addActionListener(this);
       // bWyjscie.setBackground(new Color(166,166, 166));  TO NIE DZIAŁA ALE W LINKU NIZEJ:
        //https://4programmers.net/Forum/Java/274509-java_     // opisuje jak zmieniac kolor przycisku

        twynik = new JTextField();

        twynik.setBounds(10, 300,560,30);

       add(tA);
       add(lA);
       add (tB);
       add (lB);
        add (tC);
        add (lC);
        add(bRozwiaz);
        add(bWyjscie);
         add(twynik);


    }
    public static void main (String[] args)
    {
     RownanieKwadratowe app = new RownanieKwadratowe();
     app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     app.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object zrodlo = e.getSource();

        if (zrodlo == bWyjscie)
        {
         dispose();
        }
        else if (zrodlo==bRozwiaz)
        {
         int a = Integer.parseInt(tA.getText());
         int b = Integer.parseInt(tB.getText());
         int c = Integer.parseInt(tC.getText());
         RownanieObliczenia licznik = new RownanieObliczenia(a,b,c);
         int wyniczek = licznik.oblicz();
         // https://stackoverflow.com/questions/4753339/convert-textfield-value-to-int   O KONWERSJI
         twynik.setText(String.valueOf(wyniczek));
            // https://stackoverflow.com/questions/15530370/java-have-a-int-value-using-settext TUTAJ ZNALAZLEM GORNE ROZWIAZANIE
        }
    }
}
