import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.BreakIterator;
import java.util.Scanner;

import static javax.swing.JOptionPane.showMessageDialog;


// aktualnie lekcja 17-ta
public class MojePierwszeMenuWJavce extends JFrame implements ActionListener
{ JMenuBar menuBar;
    JButton bSzukaj;
    JMenu menuPlik, menuNarzedzia,menuOpcje, menuPomoc;
    JMenuItem mOtworz, mZapisz, mWyjscie,mOpcja1, mNarz1, mNarz2, mOProgramie;   // deklaracje pól
    JTextField tSzukany;
    JCheckBoxMenuItem JChOpcja2;
    JTextArea notatnik;

    public MojePierwszeMenuWJavce()
    {

        setTitle("Demonstaracja JMenuBar");
        setSize(800,700);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);





        menuPlik = new JMenu("Plik");

        mOtworz = new JMenuItem("Otwórz", 'O'); // mnemonic w pojedynczych apostrofach
        mOtworz.addActionListener(this);
        mZapisz = new JMenuItem("Zapisz", 'Z');
        mZapisz.addActionListener(this);
        mWyjscie = new JMenuItem("Wyjście"); // dodajemy instancje liste z pozycjami do Jmenu Otwórz

        menuPlik.add(mOtworz);
        menuPlik.add(mZapisz);
        menuPlik.addSeparator();
        menuPlik.add(mWyjscie);
        mWyjscie.addActionListener(this);
        mWyjscie.setAccelerator(KeyStroke.getKeyStroke( "ctrl X"));

        menuNarzedzia = new JMenu("Narzędzia");

        mNarz1 = new JMenuItem("Narz1",'1');
        mNarz2 = new JMenuItem("Narz2",'2'); // dodajemy liste z pozycjami do Jmenu Narzedzia
        mNarz2.addActionListener(this);
        menuNarzedzia.add(mNarz1);
        menuNarzedzia.add(mNarz2);

        mNarz1.setEnabled(false);

              menuOpcje = new JMenu("Opcje");
              mOpcja1 = new JMenuItem("Opcja 1");
              JChOpcja2 = new JCheckBoxMenuItem("Opcja 2");
              JChOpcja2.addActionListener(this);
              menuOpcje.add(mOpcja1);
              menuOpcje.add(JChOpcja2);
              menuNarzedzia.add(menuOpcje);


        menuPomoc = new JMenu("Pomoc"); // instancje nowych  menu

        mOProgramie = new JMenuItem("O programie");

        menuPomoc.add(mOProgramie);
        mOProgramie.addActionListener(this);
        menuBar = new JMenuBar();

        setJMenuBar(menuBar);     // ta komenda powoduje dodanie Jmenubara czyli zamiast add piszemy to

        menuBar.add(Box.createVerticalGlue()); // nie wiem do czego słuzy
        menuBar.add(menuPlik);
        menuBar.add(Box.createHorizontalBox());
        menuBar.add(menuNarzedzia);
        menuBar.add(Box.createHorizontalGlue());  // tak to musi byc zeby zadzialalo
        menuBar.add(menuPomoc);  // dodajemy jmenu do Jmenubara


       notatnik= new JTextArea();
        JScrollPane scrollPane = new JScrollPane(notatnik);  // pola na gorze robimy, zeby sie potem do nich odwolywac
       scrollPane.setBounds(50,50,600,400);
       add(scrollPane);
        notatnik.setVisible(true);


      tSzukany =  new JTextField();
      tSzukany.setBounds(50,460, 600, 30);
      add(tSzukany);

      bSzukaj = new JButton("SZUKAJ");
      bSzukaj. setBounds(50, 500, 600, 30);
      add(bSzukaj);
      bSzukaj.addActionListener(this);








    }
    public static void main (String[] args){

        MojePierwszeMenuWJavce appMenu = new MojePierwszeMenuWJavce();
        appMenu.setVisible(true);}


    @Override
    public void actionPerformed(ActionEvent e) {
     Object z = e.getSource();

     if (z==mOtworz)
     {

         JFileChooser fc = new JFileChooser();  // mozna chyba robic w ten sposob i tak on to robi na gorze jak deklaruje komponenty
         if (fc.showOpenDialog(null)==JFileChooser.APPROVE_OPTION)
         {
           File plik = fc.getSelectedFile();
           // swiecil sie File wcisnalem ALT+enter i zaimportowalo mi klase
             //JOptionPane.showMessageDialog(null, "wybrany plik to: " + plik.getAbsolutePath());


             try {
                 Scanner scaner = new Scanner(plik);
                 while(scaner.hasNext())
                 {

                     notatnik.append(scaner.nextLine() + "\n"); //Appends the given text to the end of the document.
                                                                //dołączam dany tekst do końca dokument.

                 } scaner.close();

             } catch (FileNotFoundException e1) {
                 e1.printStackTrace();
             }


         }
     }
    else if (z==mZapisz)
     {
         JFileChooser fc = new JFileChooser();
        if (fc.showSaveDialog(null)==JFileChooser.APPROVE_OPTION)
        {
            File plik = fc.getSelectedFile();
            // swiecil sie File wcisnalem ALT+enter i zaimportowalo mi klase
           // JOptionPane.showMessageDialog(null, "wybrany plik to: " + plik);

            try {
                PrintWriter pr = new PrintWriter(plik);

                Scanner scaner = new Scanner(notatnik.getText());
                while (scaner.hasNext())
                pr.println(scaner.nextLine() + "\n" );
                pr.close();
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }

        }

     }
      else if ( z == mWyjscie) {
         int odp=  JOptionPane.showConfirmDialog(null, "Czy na pewno wyjść?", "Pytanie", JOptionPane.YES_NO_OPTION);
         if (odp== JOptionPane.YES_OPTION)
          dispose();
          else if (odp== JOptionPane.NO_OPTION)
              showMessageDialog(null, "Wiedziałem");
          else if (odp==JOptionPane.CLOSED_OPTION)
             showMessageDialog(null, "Tak nie powinno się robić ; - )", "Ważna informacja", JOptionPane.WARNING_MESSAGE);
      }
      else if(z == JChOpcja2)
      {if (JChOpcja2.isSelected()) {
          mNarz1.setEnabled(true);}
         // else
            //  mNarz1.setEnabled(false);    to moje, w filmie to robi ciekawiej

          else if (!JChOpcja2.isSelected()) // wykrzyknik pododuje zaprzecznie
          mNarz1.setEnabled(false);

      }
      else if (z==mNarz2)
      {
          String sMtry = JOptionPane.showInputDialog("Podaj długość w metrach" );
          double metry = Double.parseDouble(sMtry);
          double stopy = metry/0.3048;
          String sStopy = String.format("%.2f", stopy);  // nie dziala  w formacie zamieniamy double na string z 2 po przecinku
          showMessageDialog(null, metry + "metrow = "+ sStopy + " stóp");
      }
      else if (z== mOProgramie)
      {
          showMessageDialog(this,"Program demonstruje uzycie Menu \n Wersja 1.o", "TYTUL OKIENKA",JOptionPane.INFORMATION_MESSAGE );
      }
      else if ( z== bSzukaj) {
         String tekst = notatnik.getText();
         String szukany = tSzukany.getText();
         String wystapienia = "";
         int i = 0;
         int indeks=0;
         int startindex = 0;


         //indeks = tekst.indexOf(szukany, startindex);

         // notatnik.append(szukany);


//         if (indeks != -1) {                                                                    // test samego ifa, działa
//             JOptionPane.showMessageDialog(null, "tekst: " + (indeks));
//         }


         // if (indeks != 1);                                                        // petla do while z poczatkowym ifem sprawdzajacym

//             do  {
//                   startindex = indeks + szukany.length();
//                          i++;
//                                  wystapienia = wystapienia + " " + indeks;
//
//
//
//              }while (indeks != -1);
//


         //JOptionPane.showMessageDialog(null,"tekst: " + szukany + " Po" + indeks);

//         JOptionPane.showMessageDialog(null,"tekst: " + szukany + " Pojawia sie na pozycji: " + wystapienia);
//


          //petla While

         while (true){
         {
                     startindex = indeks + szukany.length();
                        i++;
                            wystapienia = wystapienia + " " + indeks;
                                if ((indeks = tekst.indexOf(szukany, startindex)) != -1)
                                    break;
         }
                 JOptionPane.showMessageDialog(null,"tekst: " + szukany + " Pojawia sie na pozycji: " + wystapienia);



//                                                 // moja probna petla while  działa
//         int number = 0;
//         String numers = " ";
//         while (true) {
//             number++;
//             numers = numers + " " + number;
//
//
//             if (number == 10)
//                 break;}
//
//
//             JOptionPane.showMessageDialog(null,numers);
//



}
         }




    }
}
