public class Probik_typow_danych {

    public static void main (String[] args)
    {
        System.out.println('a'+'A');
        System.out.println("a"+"A");
        System.out.println(1+2);
        System.out.println(1.0+2.0);
        System.out.println("cudzysłów \"");
        System.out.println(true);
        System.out.println("\t"+"tekst po tab");
        System.out.println("\n"+"po nowej linii"+"\n"+"tez po nowej linii");
        System.out.println("tekst"+"\r"+"??");
        System.out.println("\'");
        System.out.println("\\");

        char znak;
        for(int i = 0; i < 256;){
            ++i;
            znak = (char)i;
            System.out.print(i+" to "+znak+" ");}
    }

}
/*
\t - tab
\n - nowa linia
\r - powrót karetki
\" - cudzysłów
\' - apostrof
\\ - backslash
Ostatnim typem prostym jest boolean. Reprezentuje on tylko dwie wartości:

true - prawda
false - fałsz
 */