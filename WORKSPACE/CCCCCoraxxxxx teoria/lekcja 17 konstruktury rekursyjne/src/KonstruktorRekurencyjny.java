public class KonstruktorRekurencyjny {

    private double promien;
    private int id;
    private static int nextId;

    public KonstruktorRekurencyjny()
    {
        this(1.0);
    };

    public KonstruktorRekurencyjny(double r)
    {
        this(r,"cos");
    };


    public KonstruktorRekurencyjny ( double r, String j)
    {
        id = nextId;
        nextId++;
        promien = r;
        if (j.equals("KM"))
        { promien = promien * 1000;
        }};

    public void setPromien(double r) {
        promien = r;
    }



    public double getPromien() {
        return promien;
    }
    public double oblicz_obwod_kola()
    {
        return 2*Math.PI*promien;}

    public double oblicz_pole_kola()
    {
        double pole;
        pole= Math.PI*promien*promien;
        return pole;}

    public int getId() {
        return id;
    }

    public static void main (String[] args)
    {
     KonstruktorRekurencyjny[] kolo = new KonstruktorRekurencyjny[3];
     kolo[0]= new KonstruktorRekurencyjny();
     kolo[1]= new KonstruktorRekurencyjny(3.5);
     kolo[2]= new KonstruktorRekurencyjny(7, "KM");


    // for (KonstruktorRekurencyjny x:kolo)  // BARDZO WAZNE PRZYPISANIE DO ZAPAMIETANIA
        for (int i=0; i<kolo.length;i++)
//    { System.out.println( "Id koła wynosi" + x.getId());
//        System.out.println( "Promien koła wynosi" + x.getPromien());
//       System.out.println("Obwód koła wynosi" + x.oblicz_obwod_kola());
//       System.out.println("Oblicz pole koła" + x.oblicz_pole_kola());
//    }
          // ponizej moj spoos ale wazniejszy jest do zapamietania ten zakomentowany z filmu
        { System.out.println( "Id koła wynosi" + kolo[i].getId());
            System.out.println( "Promien koła wynosi" + kolo[i].getPromien());
            System.out.println("Obwód koła wynosi" + kolo[i].oblicz_obwod_kola());
            System.out.println("Oblicz pole koła" + kolo[i].oblicz_pole_kola());
        }


    }
    static
    {
        nextId = 1;
    }
}
