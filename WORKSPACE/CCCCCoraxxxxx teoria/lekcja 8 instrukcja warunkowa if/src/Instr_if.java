public class Instr_if {
    public static void main (String[] args) {
        int x= 103;
        if (x==10)
        {
            System.out.println("x wynosi 10");
        }
        else
        {
            System.out.println("x wynosi " + x);
        }
        if (x>0)
        {
            System.out.println("wieksze od 0");
        }
        else if (x<0)
        {
            System.out.println("x<0");
        }
        else
        {
            System.out.println("x=103"); // jezeli chcesz sprawdzic wiecej warunków  uzyj kilku "if"
        }
        int c=4;
        int cond = (x>0)?1:0;
        System.out.println("warunek cond wynosi " + cond);

    }
}
