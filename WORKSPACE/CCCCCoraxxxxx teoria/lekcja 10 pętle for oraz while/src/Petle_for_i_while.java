public class Petle_for_i_while {
    public static void main (String[] args)
    {
for (int i=1; i<=10; i=i+2) // zamiast i++ dałem i=i+2;
{
    System.out.println(i);
}
        for (int a=10; a>=1; a=a-2) // zamiast i++ dałem i=i+2;
    {
        System.out.println(a);
    }
        for (double x=2; x>=1; x=x-0.2) // zamiast i++ dałem i=i+2;
        {
            System.out.println(x);
        }

        // petla while
        int y=1;
        while (y<=10)
        {
            System.out.println(y);
            y++;
        }

        // petla do while
         int s=1;

        do {

            System.out.println(s);
            s++;
        }
        while (s<=10);
        // petla do while z false
        int l=1;

        do {

            System.out.println(l);
            l++;
        }
        while (false); // petla sie i tak raz wykona
        //

    }
}
