public class Lancuchy_tekstowe {
    public static void main (String[] args)
    {
    String s = "Szła dziewieczka do laseczka, szla i szla";
        System.out.println(s.length());
        System.out.println(s.substring(5));
        System.out.println(s.substring(5,10));
        System.out.println(s.indexOf("dziew")); // wyszukaj na jakiej pozycji sie znajduje

        // wyszukje na ktorej pozycji jest "szła"
        System.out.println(s.indexOf("szla"));
        // teraz skracamy lancuch
        System.out.println(s.substring(30));

        System.out.println(s.lastIndexOf("szla")); // ostatnie wystapienie lancucha
      // == nie uzywamy do porownywania lancuchow tylko equlas

        String a= "kot";
        System.out.println(a.equals("lis"));

        // zmiennej s nie mozemy zmieniac, ale mozeny jej zrobic nowe przypisanie
       s = s.substring(30);
        System.out.println(s);
    }
}
