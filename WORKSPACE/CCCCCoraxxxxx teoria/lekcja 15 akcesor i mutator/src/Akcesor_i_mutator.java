public class Akcesor_i_mutator {
    private double promien;

    public void setPromien(double r) {
         promien = r;
    }

    public double getPromien() {
        return promien;
    }
    public double oblicz_obwod_kola()
    {
        return 2*Math.PI*promien;}

    public double oblicz_pole_kola()
    {
        double pole;
        pole= Math.PI*promien*promien;
        return pole;}

    public static void main (String[] args) {

        Akcesor_i_mutator pole_promien = new Akcesor_i_mutator();
        {
        pole_promien.setPromien(7);
            System.out.println("Promien koła wynosi " + pole_promien.getPromien());
            System.out.println("Obwod koła wynosi " + pole_promien.oblicz_obwod_kola());
            System.out.println("Pole koła wynosi " + pole_promien.oblicz_pole_kola());
        }

    }
}
