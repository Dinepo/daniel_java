import javax.sound.midi.Soundbank;

public class TabliczkaMnożenia {
    double a;
    double b;

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double mnożenie()
    {
        return a* b;
    }

    public double dodawanie()
    {
        return a+b;
    }
    public double odejmowanie()
    {
        return a-b;

    }

    public double dzielenie()
    {
        return a/b;

    }

    public static void main (String[] args)
    {
TabliczkaMnożenia Tabliczka = new TabliczkaMnożenia();

Tabliczka.setA(5);
Tabliczka.setB(50);

System.out.println( " a i b= " + Tabliczka.getA() + " " + Tabliczka.getB()+ "  mnozenie  " + Tabliczka.mnożenie()+ "  dzielenie  " + Tabliczka.dzielenie());
    }


}

