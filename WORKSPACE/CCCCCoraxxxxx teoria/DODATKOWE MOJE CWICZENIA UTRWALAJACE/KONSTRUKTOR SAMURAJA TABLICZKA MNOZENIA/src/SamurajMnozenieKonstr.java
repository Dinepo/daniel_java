public class SamurajMnozenieKonstr {

    private double a;
    private double b;

    public SamurajMnozenieKonstr ( double x,double y )
    {
        a=x;
        b=y;
    }

    public double wynikSamuraja()
    {
        return a*b;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

}
