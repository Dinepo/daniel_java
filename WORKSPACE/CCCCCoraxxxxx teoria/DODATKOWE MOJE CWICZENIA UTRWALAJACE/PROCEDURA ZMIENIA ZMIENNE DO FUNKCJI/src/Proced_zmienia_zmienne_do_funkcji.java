import java.util.Arrays;
import java.util.Scanner;

public class Proced_zmienia_zmienne_do_funkcji {
    static int zmienna1;  // cholera wie, czemu tu musi byc static
    static int zmienna2;

    public static void zmien(int x, int y) // procedura ktora zmieni wartosc zmiennycj
    {
        zmienna1 = x;
        zmienna2 = y;

    }

    public static int wynik(int a, int b) {
        return a * b;
    }

    public static void main(String[] args) {
        //Proced_zmienia_zmienne_do_funkcji procka1 = new Proced_zmienia_zmienne_do_funkcji();
        Scanner input = new Scanner(System.in);
        System.out.println("podaj zmienna 1");
        zmienna1 = input.nextInt();
        System.out.println("podaj zmienna 2");
        zmienna2 = input.nextInt();
        // uzycie procedury do zmiany zmiennych
        for (int i = 1; i < 5; i = i + 2) {
            System.out.println(wynik(zmienna1, zmienna2) + "a " + i);
        }

        zmien(10, 4);

        int w = 1;

        while (w <= 2) {
            System.out.println(wynik(zmienna1, zmienna2) + "b " + w);
            w = w + 1;

        }

        zmien(6,7);
        int d = 1;

        do{
            System.out.println(wynik(zmienna1, zmienna2) + "c " + d);
        d++;} // w tym miejscu podnosimy petle wewnatrz do
            while ( d<3);


        int[] tablica = {8,9,11};
        for (int x : tablica)
        {
            System.out.println(x);
        }
        for (int u = 0; u<3; u++)
        {
            System.out.println(tablica[u]);

        }

        System.out.println(Arrays.toString(tablica));

        int[] kopia = new int [5];
        kopia = Arrays.copyOf(tablica, tablica.length);
        tablica[1]= 1000;
        Arrays.sort(tablica);
        System.out.println(Arrays.toString(tablica));
        System.out.println(Arrays.toString(kopia));

    }



}

// wczesniej mialem obiekt, ale dzieki dodaniu static do metod nie trzeba obiektu