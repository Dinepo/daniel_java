public class Lekcja18Rownanie {

    private int a;
    private int b;
    private int c;

    public Lekcja18Rownanie(int a,int b,int c)
    {
        this.a = a;
        this.b = b;
        this.c = c;


    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getC() {
        return c;
    }

    public String rozwiaz()
    {    int policz;
        policz = getA() * getB() * getC();
        String rozwiazanie = "";
        rozwiazanie = "wynik to " + policz;
  return rozwiazanie;
    }

   public String toString()         // modyfikujemy metode toString z klasy object
    {
        return "liczby to :" + getA() + "" + getB()+ "" + getC() + "modyfikujemy metode toString z klasy object ";
    }

    public void setA(int a) {
        this.a = a;
    }

    public void setB(int b) {
        this.b = b;
    }

    public void setC(int c) {
        this.c = c;
    }

    public static void main (String[] args)
    {
        Lekcja18Rownanie obiekt18 = new Lekcja18Rownanie(2,2,2);
        System.out.println(obiekt18.rozwiaz());
    }
}
