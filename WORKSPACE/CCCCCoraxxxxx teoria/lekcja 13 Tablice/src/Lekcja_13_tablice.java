import java.util.Arrays;

public class Lekcja_13_tablice {
    public static void main(String[] args) {
        // musisz użyć operatora new, kiedy nie określasz z góry składowych elementów tablicy, żeby kompilator wiedział ile ich będzie, w Java tablice mają predefiniowaną wielkość dlatego int[] kopia = new int[wielkość];
////"new int[5]" oznacza tyle co "pięć nowych, pustych miejsc".
        //Puste miejsce nie jest równe 0. ﻿
        //public static void main (String[] args)
        {System.out.println("for (int i = 0; i < tablica.length; i++)     ");

            int[] tablica = {1, 4, 2};
            for (int i = 0; i < tablica.length; i++) {
                System.out.println(tablica[i]);
            }


        }
        System.out.println("     ");
        System.out.println(" for (int x:tablica1)     ");
        // petla roszerzona

        int[] tablica1 = {4, 3, 2};

        for (int x:tablica1)
        {
            System.out.println(x);}
        System.out.println("     ");
        System.out.println(" Arrays.toString(tablica1)   ");
        System.out.println(Arrays.toString(tablica1));
        System.out.println("     ");
        System.out.println(" Tworzymy kopie tablicy ze zmienionym elementem [0]    ");

        int kopia[]= new int[5];
        kopia=tablica1;
        tablica1[0]= 1000; // przypisuje pierwszej tablicy inny element
        System.out.println(Arrays.toString(tablica1));
        System.out.println(Arrays.toString(kopia));
        // efekt wydruku jest taki, że nie zmieniło sie nic poniewaz wskazuja na ten sam adres:
        //[1000, 3, 2]
        //[1000, 3, 2]
        System.out.println("     ");
        System.out.println(" Uzyskujemy prawdziwa wartosc tablicy     ");

        int kopia1[]= new int[5];
        int[] tablica2 = {500, 300, 200, 700, 300};
        kopia1 = Arrays.copyOf(tablica2,tablica2.length);
        tablica2[1]=2000;
        System.out.println(Arrays.toString(tablica2));
        System.out.println(Arrays.toString(kopia1));
         // nie działa w tej wersji
        // wynik:
        //[1000, 3, 2, 7, 3]
        //[1000, 3, 2, 7, 3]
        System.out.println("     ");
        System.out.println(" Uzyskujemy prawdziwa wartosc z sortowaniem ");
        //to teraz zrobimy inaczej
        int kopia2[]= new int[5];
        kopia2 = Arrays.copyOf(tablica2,tablica2.length);
        tablica2[0]= 1000; // przypisuje pierwszej tablicy inny element
        Arrays.sort(kopia2);
        System.out.println(Arrays.toString(tablica2));
        System.out.println(Arrays.toString(kopia2));


        // nie działa na tej wersji
    }
}
