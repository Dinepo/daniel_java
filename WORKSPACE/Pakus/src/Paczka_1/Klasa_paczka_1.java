package Paczka_1;

public class Klasa_paczka_1 {

    @Override
    public String toString() {
        return "Klasa_paczka_1{" +
                "a=" + a +""+funNaPotrzebytoString()+
                '}';
    }

    public String funNaPotrzebytoString() {
        return "akuku";
    }

    public int a=5;


    public static void main(String[] args) {

        test.a = 4;

        bok_1.zwrotka_ze_static(); // widzi ta metode bo bok_1 jest w tej samej paczce i ma tam static
                     // w klasa_paczka_2 tez widzi bo jest static dziwne nie ?

        bok_1.zwrotka_ze_static();// w paczce działa nawet jak usunałem słówko public przed bok_1;

        bok_1 boczunio = new bok_1("Daniel", "Pałubski");
        boczunio.zwrotka_bez_static_ale_instancja(); // wywolujemy metode bez static dzieki instancji (nawet w tej samej paczce musi byc instancja

        System.out.println( boczunio.toString());
    }
}
