package Paczka_2;

import Paczka_1.*;

public class Klasa_paczka_2 {

    public static void main(String[] args) {

  // test.  zeby widzial a musi byc public przy zmiennej a bo to inny pakunek i klasa test musi byc w osobnej zakladce jako public

      Klasa_paczka_1 klasunia1 = new Klasa_paczka_1();  // instancja

        System.out.println( klasunia1.toString());


                bok_1.zwrotka_ze_static(); // oki juz wiem, zeby tutaj byla procedura zwrotka to w paczka_1.bok_1 musi ta metoda byc statyczna
                                       // inaczej jej w gole nie widzi zauwaz ze to inna paczka
               // juz wiem usun w bok1 przed class słówko public i bedzie wtedy blokada a klasa_paczka_1 bedzie dostepne


        bok_1 boczek = new bok_1("danilko", "Palubski");
        boczek.zwrotka_bez_static_ale_instancja(); // tutaj tworze instacje klasy i wtedy widzi metode bez static w bok_1

        boczek.setA(12);
        System.out.println("tym razek bok a wynosi " + boczek.getA());
    }
}
