import javafx.stage.Popup;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Scanner;

public class Dwa_jTextarea extends JFrame implements ActionListener{
  JTextArea okno1;
  JTextArea okno2;
  JPopupMenu popup;
  JMenuItem mpPrzeslij, mpKopiuj;
  JMenuBar menuBar;
  JMenu menuPlik, menuNarzedzia, menuPomoc, menuOpcje;
  JMenuItem mOtworz, mZapisz, mWyjscie, mDrukuj, mWyslij;

  String bufore;
  String bufore2;
    String odczyt ="";


    public Dwa_jTextarea()
    {

        menuBar = new JMenuBar();
        menuOpcje = new JMenu("Opcje dodatkowe");
        menuPlik = new JMenu("PLIK");
        menuNarzedzia = new JMenu("NARZĘDZIA");
        menuPomoc = new JMenu("POMOC");
        mOtworz = new JMenuItem("OTWORZ");
        mOtworz.addActionListener(this);
        mZapisz = new JMenuItem("ZAPISZ");
        mZapisz.addActionListener(this);
        mWyjscie = new JMenuItem("WYJSCIE");
        mDrukuj = new JMenuItem("DRUKUJ");
        mWyslij = new JMenuItem("WYSLIJ");


        setJMenuBar(menuBar);

        menuBar.add(menuPlik);
        menuBar.add(menuNarzedzia);
        menuBar.add(menuPomoc);

        menuOpcje.add(mDrukuj);
        menuOpcje.add(mWyslij);

        menuPlik.add(mOtworz);
        menuPlik.addSeparator();
        menuPlik.add(menuOpcje);
        menuPlik.addSeparator();
        menuPlik.add(mZapisz);
        menuPlik.addSeparator();
        menuPlik.add(mWyjscie);

        setTitle("PRÓBNIK DO PROGRAMU LISTA PODRÓZNA");
        setLocation(200,0);
        setSize(700,800);
        setLayout(null);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    okno1 = new JTextArea();
    okno2 = new JTextArea();


        okno1 = new JTextArea("Nożyczki \nNóż \nWidelec \nNamiot ");
        JScrollPane sp = new JScrollPane(okno1);
        sp.setBounds(0, 150, 250, 500);
        Font font = new Font("Verdana", Font.BOLD, 12);
        okno1.setFont(font);
        okno1.setForeground(Color.BLUE);
        okno1.setToolTipText("pierwsze okno ");
        add(sp);
        okno1.setVisible(true);


        okno2 = new JTextArea();
        JScrollPane sp2 = new JScrollPane(okno2);
        sp2.setBounds(350, 150, 250, 500);
        Font font2 = new Font("Verdana", Font.BOLD, 12);
        okno2.setFont(font);
        okno2.setForeground(Color.BLUE);
        okno2.setToolTipText("drugie okno ");
        add(sp2);
        okno2.setVisible(true);

        popup = new JPopupMenu();
        mpKopiuj = new JMenuItem("KOPIUJ");
        mpKopiuj.addActionListener(this);

        mpPrzeslij = new JMenuItem("PRZESLIJ");
        mpPrzeslij.addActionListener(this);

        popup.add(mpKopiuj);
        popup.add(mpPrzeslij);


okno1.setComponentPopupMenu(popup);

    }

    public static void main (String[] args)
    {
     Dwa_jTextarea memka = new Dwa_jTextarea();
     memka.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object z = e.getSource();
        if (z==mOtworz)
        {
            JFileChooser fc = new JFileChooser();
            if (fc.showOpenDialog(null)==JFileChooser.APPROVE_OPTION)
            {
                File plik = fc.getSelectedFile();
                JOptionPane.showMessageDialog(null, "wybrany plik to " + plik.getName());


                // ten sposób dzialał ale dokleja linie w poziomie, zrobiłem konstrukcje z doatkowym \n
                // która zalatwia sprawe :)
                try (Scanner odczytaj = new Scanner(plik)) {


                    okno1.append("\n"); // przesuniecie lini
                    while (odczytaj.hasNext())


                    okno1.append(odczytaj.nextLine() + "\n");}
                 catch (FileNotFoundException e1) {
                    e1.printStackTrace();}

//                try {  // ten sposos zastepuje zawartosc w text array
//
//                    okno1.read( new FileReader( plik.getAbsolutePath() ), null );

//                } catch (IOException u) {
//
//                    System.out.println("Nie mogę otworzyć pliku: "+plik.getAbsolutePath());
//
//                    System.out.println("Problem: "+u);
//
//                }

            } // http://ggoralski.pl/?p=970  TUTAJ JEST SUPER SPOSOB NA WPISYWANIE DO MEMA

        }
        else if (z == mZapisz){
            JFileChooser fc = new JFileChooser();
        if (fc.showOpenDialog(null)==JFileChooser.APPROVE_OPTION) {
            File plik = fc.getSelectedFile();



            //JOptionPane.showMessageDialog(null, "wybrany plik to " + plik.getName());}}
            try (PrintWriter pw = new PrintWriter(plik)) {


                //pw.println(okno1.getText() + "\n"); // \n sam dodalem ale to bez sensu

                // to był na gorze zapis wg ksiazki mimo ze \n jest w lini jednej :
                // https://javastart.pl/baza-wiedzy/darmowy-kurs-java/podstawy-jezyka/zapis-i-odczyt-z-plikow

                // teraz zapis weglug coraxa https://www.youtube.com/watch?v=jD0ya9TESfI  11.05 -czas filmu

                Scanner sc = new Scanner(okno1.getText());
                while (sc.hasNext())
                {
                   pw.println(sc.nextLine());
                }



            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }


        }
        else if (z == mpPrzeslij){



            bufore = okno1.getSelectedText();

            int poz_kur = okno2.getCaretPosition();


            if (poz_kur == 0){
            okno2.insert( bufore,okno2.getCaretPosition());
             }
            else

            okno2.append("\n" + bufore);} // moja konstrukcja do wklejania do memo zeby skakało nizej

          else if(z==mpKopiuj)
            {
                final Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
                String selected = okno1.getSelectedText();
                StringSelection data = new StringSelection(selected);
                clip.setContents(data, data);
            }
        }

    }}

