import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class odczyt{
    public static void main(String[] args) throws FileNotFoundException{
        File pliczek = new File("C:\\Users\\Lenovo\\Desktop\\REPO JAVA\\WORKSPACE\\odczyt_scanner\\out\\production\\odczyt_scanner\\ala.txt");

        // tworzymy obiekt przechowujacy dane czyli pliczek
        Scanner moj_odczyt = new Scanner(pliczek); // odczytujemy pliczek


// inny sposob:
        //Scanner moj_odczyt = new Scanner(new File("C:\\Users\\Lenovo\\Desktop\\REPO JAVA\\WORKSPACE\\odczyt_scanner\\out\\production\\odczyt_scanner\\ala.txt"));



        String moje_zdanie = moj_odczyt.nextLine();// wczytujemy do zmiennej dane w pliku tekstowego
        System.out.println(moje_zdanie);
    }
}